const cacheName = 'sw-v1';
const cacheFiles = [
    './',
    'styles.css',
    'script.js'
];

self.addEventListener ('install', event => {

    console.log('[Service Worker] Entered Installed Phase')

    event.waitUntil (
      // Open a cache…
      caches.open(cacheName).then (cache => {
        // And add resources to it
        console.log('[Service Worker] Caching cacheFiles')
        return cache.addAll(cacheFiles)
      })
    )
})

self.addEventListener ('activate', event => {

    console.log('[Service Worker] Entered Activate Phase')

    // Delete non-current cached files
    event.waitUntil (
      caches.keys().then(cacheNames => {
      	return Promise.all(cacheNames.map(thisCacheName => {
          if (thisCacheName !== cacheName) {
            console.log('[Service Worker] Removing cached files', thisCacheName)
            return caches.delete(thisCacheName)
      	  }
      	}))
      })
    )
})

self.addEventListener ('fetch', event => {

    console.log('[Service Worker] Entered Fetch Phase')

    event.respondWith (
      caches.match(event.request).then(response => {
        return response || fetch(event.request)
      })
    )
})
