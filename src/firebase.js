import Firebase from 'firebase'

const firebaseApp = Firebase.initializeApp({
  apiKey: 'AIzaSyDtlYJ3-YNnL0GVlI3Ph6DldLKeXMkeBbg',
  authDomain: 'uxc-pwa.firebaseapp.com',
  databaseURL: 'https://uxc-pwa.firebaseio.com'
})

export const db = firebaseApp.database()
